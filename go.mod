module git.glasklar.is/rgdd/hostcfg

go 1.21.3

require (
	github.com/vishvananda/netlink v1.1.1-0.20211118161826-650dca95af54
	system-transparency.org/stboot v0.3.1
)

require (
	github.com/vishvananda/netns v0.0.0-20210104183010-2eb08e3e575f // indirect
	golang.org/x/sys v0.0.0-20220610221304-9f5ed59c137d // indirect
)
