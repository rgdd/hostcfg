README
======

Below, `/path/to/input/file` is the path to a file that contains one JSON host
configuration per line.

    $ go run cmd/hostcfg-debug/main.go -f /path/to/input/file
    Number of host configurations:     417
    Missing "bonding_mode":            0
    Missing "bond_name":               0
    Missing "ospkg_pointer":           417  (have "provisioning_urls" with one url)
    Missing "network_interfaces":      0    (have "network_interface")
    Incompatible "network_interfaces": 74   (want list of ifname and ifaddr tuples)
    Incompatible "dns":                417  (want list of ip addresses)
    Set keys that are being ignored:   map[bonding:417 network_interface:417 provisioning_urls:417 timestamp:417 version:417]
    $
    $
    $ go run cmd/hostcfg-debug/main.go -f /path/to/other/file
    Number of host configurations:     205
    Missing "bonding_mode":            18
    Missing "bond_name":               18
    Missing "ospkg_pointer":           205  (have "provisioning_urls" with one url)
    Missing "network_interfaces":      18   (have "network_interface")
    Incompatible "network_interfaces": 65   (want list of ifname and ifaddr tuples)
    Incompatible "dns":                205  (want list of ip addresses)
    Set keys that are being ignored:   map[bonding:187 network_interface:205 provisioning_urls:205 timestamp:205 version:205]

How was the above concluded?

    $ git diff 79add0a6ba586fda6910ebd06d058e2a88b4f2fe
    $ git diff 13caa06f6da91546a89b72ff5ee014fcf0b2d9ad

Or try to just get the gist by looking at:

    $ less pkg/host/monkey-patch.go

Warning: the above observations are based on "static" configurations only.
E.g., it may be the case that there are incompatibilities with old dhcp
configurations.  It may also be the case that there are more incompatibilities
that just didn't show up in the sample of analyzed "static" configurations.

For example, all the analyzed "static" configurations were setting dummy values
for "identity" and "authentication", which means they are present and valid.

---

If you want to plot when these configurations were provisioned, run:

    $ ./scripts/timeplot.py /path/to/input/file provisioned_at.png

See the generated histogram in `provisioned_at.png`.

---

If you want to check your configs with stmgr:

    $ ./scripts/checkall.sh /path/to/input/file

---

To answer the question of if there are any configurations that set both
"network_interface" and "network_interfaces" without setting bonding:

    $ (cd scripts && ./check-assumption.py)
    num_all: 622
    num_nic: 622
    num_nics: 157
    num_bonding: 139
    num_nic_and_nics: 622
    num_nic_and_nics_no_bonding: 0

In other words, if it simplifies the solution towards backwards-compatibility to
assume that having "network_interfaces" trumps "network_interface" because
bonding is always enabled, then that seems to be a reasonable assumption.

Be warned (similar to above) -- this type of analysis assumes that the host
configurations we're looking at are representative of deployed host configs.

nisse also did a similar analysis with jq:

> Trying this query:
> 
> jq 'select(.bonding != true)|.network_interfaces < configs.txt
> 
> and I get only null values.
