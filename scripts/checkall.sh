#!/bin/bash

set -eu

file_path=$1; shift

num_lines=0
num_valid=0
while IFS= read -r line; do
	num_lines=$(( num_lines + 1 ))
	if ! stmgr hostconfig check "$line" >/dev/null; then
		continue
	fi

	num_valid=$(( num_valid + 1 ))
done <"$file_path"

echo "$num_valid/$num_lines host configs parse"
