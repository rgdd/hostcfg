#!/usr/bin/env python3

import os
import json

DIR = "/home/rgdd/Downloads/st"

num_all = 0
num_nic = 0
num_nics = 0
num_bonding = 0

num_both = 0
num_both_no_bonding = 0

for filename in os.listdir(DIR):
    file_path = os.path.join(DIR, filename)
    with open(file_path, 'r') as file:
        for line in file:
            try:
                obj = json.loads(line)
                nic = obj.get("network_interface", "")
                nics = obj.get("network_interfaces", [])
                bm = obj.get("bonding_mode", "")

                num_all += 1
                if nic != "":
                    have_nic = True
                    num_nic += 1
                if nics is not None:
                    have_nics = True
                    num_nics += 1
                if bm != "":
                    have_bonding = True
                    num_bonding += 1

                if have_nic and have_nics:
                    num_both += 1
                    if not have_bonding:
                        num_both_no_bonding += 1

            except json.JSONDecodeError:
                print(f"Warning: Could not decode JSON from line in file {filename}")

print(f'num_all: {num_all}')
print(f'num_nic: {num_nic}')
print(f'num_nics: {num_nics}')
print(f'num_bonding: {num_bonding}')
print(f'num_nic_and_nics: {num_both}')
print(f'num_nic_and_nics_no_bonding: {num_both_no_bonding}')
