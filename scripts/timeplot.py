#!/usr/bin/env python3

import json
import datetime
import matplotlib.pyplot as plt
import argparse

def read_timestamps_from_file(file_path):
    timestamps = []
    with open(file_path, 'r') as file:
        for line in file:
            data = json.loads(line)
            timestamps.append(data.get('timestamp'))
    return timestamps

def convert_to_readable_dates(timestamps):
    # Sort timestamps to ensure ascending order
    return sorted([datetime.datetime.fromtimestamp(ts) for ts in timestamps])

def plot_timestamps(timestamps, output_file):
    # Converting timestamps to 'YYYY-MM' format for monthly binning
    months = [dt.strftime('%Y-%m') for dt in timestamps]
    # Create bins for each unique month in sorted order
    bins = sorted(set(months))
    plt.hist(months, bins=bins, edgecolor='black')
    plt.xticks(rotation=45)
    plt.xlabel('Month')
    plt.ylabel('Number of Configurations')
    plt.title('Configurations Over Time')
    plt.tight_layout()
    plt.savefig(output_file, format='png')  # Save the plot as a PNG file

def parse_arguments():
    parser = argparse.ArgumentParser(description='Process JSON files to analyze timestamps.')
    parser.add_argument('file_path', type=str, help='Path to the JSON file')
    parser.add_argument('output_file', type=str, help='Path to save the output plot')
    return parser.parse_args()

def main():
    args = parse_arguments()
    timestamps = read_timestamps_from_file(args.file_path)
    readable_dates = convert_to_readable_dates(timestamps)
    plot_timestamps(readable_dates, args.output_file)

if __name__ == '__main__':
    main()
