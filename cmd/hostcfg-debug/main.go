package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	"git.glasklar.is/rgdd/hostcfg/pkg/host"
)

func main() {
	filename := flag.String("f", "", "File with one JSON host configuration per line")

	flag.Parse()
	if *filename == "" {
		log.Fatal("provide the path to a file with -f")
	}

	file, err := os.Open(*filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	r := report{}
	lineNum := 1
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		incompatibilities, err := processOne(scanner.Text())
		if err != nil {
			log.Fatalf("line %d: %v\n", lineNum, err)
		}

		r = append(r, incompatibilities)
		lineNum += 1
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(os.Stdout, "%s", r.Summary())
}

func processOne(line string) (incompatibilities, error) {
	var cfg host.Config
	if err := json.Unmarshal([]byte(line), &cfg); err != nil {
		return incompatibilities{}, err
	}

	obsoleteKeys := make([]string, len(host.ObsoleteKeys))
	copy(obsoleteKeys, host.ObsoleteKeys)
	return incompatibilities{
		missingOSPkgPointer: *cfg.OSPkgPointer == host.FixedURL,
		missingBondingMode:  cfg.BondingMode == host.FixedBondingMode,
		missingBondName:     *cfg.BondName == host.FixedBondName,
		missingNetworkInterfaces: cfg.NetworkInterfaces != nil && func() bool {
			if len(*cfg.NetworkInterfaces) != 1 {
				return false
			}
			ni := (*cfg.NetworkInterfaces)[0]
			if *ni.InterfaceName != host.FixedIfnameSingle {
				return false
			}
			if !bytes.Equal(*ni.MACAddress, host.FixedIfaddrSingle) {
				return false
			}
			return true
		}(),
		brokenNetworkInterfaces: cfg.NetworkInterfaces != nil && func() bool {
			for _, ni := range *cfg.NetworkInterfaces {
				if *ni.InterfaceName != host.FixedIfname {
					return false
				}
				if !bytes.Equal(*ni.MACAddress, host.FixedIfaddr) {
					return false
				}
			}
			return true
		}(),
		brokenDNSServer: cfg.DNSServer != nil &&
			len(*cfg.DNSServer) == 1 &&
			bytes.Equal(*((*cfg.DNSServer)[0]), host.FixedDNS),
		obsoleteKeys: obsoleteKeys,
	}, nil
}

type incompatibilities struct {
	missingOSPkgPointer      bool
	missingBondingMode       bool
	missingBondName          bool
	missingNetworkInterfaces bool
	brokenNetworkInterfaces  bool
	brokenDNSServer          bool
	obsoleteKeys             []string
}

type report []incompatibilities

func (r *report) Summary() string {
	numMissingOSPkgPointer := 0
	numMissingBondingMode := 0
	numMissingBondName := 0
	numMissingNetworkInterfaces := 0
	numBrokenNetworkInterfaces := 0
	numBrokenDNSServer := 0

	obsoleteKeys := make(map[string]int)
	for _, incompatibilities := range *r {
		if incompatibilities.missingOSPkgPointer {
			numMissingOSPkgPointer += 1
		}
		if incompatibilities.missingBondingMode {
			numMissingBondingMode += 1
		}
		if incompatibilities.missingBondName {
			numMissingBondName += 1
		}
		if incompatibilities.missingNetworkInterfaces {
			numMissingNetworkInterfaces += 1
		}
		if incompatibilities.brokenNetworkInterfaces {
			numBrokenNetworkInterfaces += 1
		}
		if incompatibilities.brokenDNSServer {
			numBrokenDNSServer += 1
		}

		for _, key := range incompatibilities.obsoleteKeys {
			v, ok := obsoleteKeys[key]
			if !ok {
				obsoleteKeys[key] = 1
			} else {
				obsoleteKeys[key] = v + 1
			}
		}
	}

	return fmt.Sprintf("Number of host configurations:     %d\n", len(*r)) +
		fmt.Sprintf("Missing \"bonding_mode\":            %d\n", numMissingBondingMode) +
		fmt.Sprintf("Missing \"bond_name\":               %d\n", numMissingBondName) +
		fmt.Sprintf("Missing \"ospkg_pointer\":           %d\t(have \"provisioning_urls\" with one url)\n", numMissingOSPkgPointer) +
		fmt.Sprintf("Missing \"network_interfaces\":      %d\t(have \"network_interface\")\n", numMissingNetworkInterfaces) +
		fmt.Sprintf("Incompatible \"network_interfaces\": %d\t(want list of ifname and ifaddr tuples)\n", numBrokenNetworkInterfaces) +
		fmt.Sprintf("Incompatible \"dns\":                %d\t(want list of ip addresses)\n", numBrokenDNSServer) +
		fmt.Sprintf("Set keys that are being ignored:   %v\n", obsoleteKeys)
}
