package host

import (
	"encoding/json"
	"fmt"
	"net"
)

var (
	// ObsoleteKeys are keys that weren't parsed when calling json.Unmarshal on
	// a host configuration.  It is reset every time a new host configuration is
	// parsed, as it is a hacky global variable.  Warning warning...!
	//
	// Looks like we're finding these unused keys:
	//
	//   "bonding"
	//        Dropped here:
	//        https://git.glasklar.is/system-transparency/core/stprov/-/commit/54240d178c78b8f8d907c4e12817a96c3d837926
	//
	//        Can't find any trace of this ever being in stboot.
	//
	//   "network_interface"
	//      Dropped here:
	//      https://git.glasklar.is/system-transparency/core/stboot/-/commit/c3ccf5555caa09578100b0a6f3c71b28f74e1da0
	//      https://git.glasklar.is/system-transparency/core/stprov/-/commit/54240d178c78b8f8d907c4e12817a96c3d837926
	//
	//   "provisioning_urls":
	//       Dropped here:
	//       https://git.glasklar.is/system-transparency/core/stboot/-/commit/9b8fc85fd7f041663c7a76b9ffe211375ef96b1b
	//       https://git.glasklar.is/system-transparency/core/stprov/-/commit/c0020ca4ac7fc5e350249d4932e2be3353da2fc9
	//
	//   "timestamp":
	//       Dropped here:
	//       https://git.glasklar.is/system-transparency/core/stboot/-/commit/23b78d00f567925dba7b4870116ed77b6c014d50
	//       https://git.glasklar.is/system-transparency/core/stprov/-/commit/54240d178c78b8f8d907c4e12817a96c3d837926
	//
	//   "version":
	//      Dropped here:
	//      https://git.glasklar.is/system-transparency/core/stboot/-/commit/c1e1ff9cea158df194c31262de13c60172ff689c
	//      https://git.glasklar.is/system-transparency/core/stprov/-/commit/50a47bcc5c6907bf76e6b84edbc18c0092b34a55
	//
	// Note that these keys would simply be ignored by stboot and not cause
	// incompatibilities in and of themselves.  Actual incompatibilities below.
	ObsoleteKeys = []string{}

	// The field "provisioning_urls" (list of strings) was changed to
	// "ospkg_pointer" (string, generalization for both initramfs path and
	// provisioning URLs; ignoring intiramfs as we're not analyzing that), see:
	// https://git.glasklar.is/system-transparency/core/stboot/-/commit/9b8fc85fd7f041663c7a76b9ffe211375ef96b1b
	//
	// The current stboot will fail if it cannot find this key.
	//
	// "ospkg_pointer" expects URLs to be a comma-separated list, see:
	// https://git.glasklar.is/system-transparency/core/stboot/-/blob/d576dc81c6c6e0e49c689deb556fc791be2e5e5d/stboot.go#L620
	//
	// If "ospkg_pointer" is missing, we monkey-patch it here with FixedURL iff
	// "provisioning_urls" contains exactly one string in its list.
	FixedURL = "http://example.org/ospkg.json"

	// "network_interfaces" (list of strings) was introduced for bonding, see:
	// https://git.glasklar.is/system-transparency/core/stboot/-/commit/334d268333f0ed7a4bda02cff561c07c7ed3c269
	//
	// It was then changed to list of objects, where each object is a an
	// interface name (string) and mac address (string aa:bb:cc:dd:ee:ff), see:
	// https://git.glasklar.is/system-transparency/core/stboot/-/commit/c3ccf5555caa09578100b0a6f3c71b28f74e1da0
	//
	// The current stboot will fail to parse this entry.
	//
	// If we're unable to parse as the new two-tuple of name and address, we
	// monkey-patch it here with FixedIfname and FixedIfaddr.
	FixedIfname = "dummy-ifname"
	FixedIfaddr = net.HardwareAddr{1, 1, 1, 1, 1, 1}

	// "network_interface" is obsolete, so if there's a host configuration that
	// sets this without possibly also setting "network_interfaces" that will
	// also result in a parsing error.  If this happens, monkey match as below.
	FixedIfnameSingle = "dummy-ifname-single"
	FixedIfaddrSingle = net.HardwareAddr{2, 2, 2, 2, 2, 2}

	// "dns" (string 1.2.3.4) was changed to list of strings to support multiple
	// DNS servers, see:
	// https://git.glasklar.is/system-transparency/core/stboot/-/commit/8d38269e88242a714edb1fd8623950900c40205c
	//
	// The current stboot will fail to parse this entry.
	//
	// If we're unable to parse as a list of string, we try to parse as a string
	// and monkey patch with FixedDNS here (resulting in a list of length 1).
	FixedDNS = net.IP{100, 101, 102, 103}

	// Added as new fields here:
	//
	//       https://git.glasklar.is/system-transparency/core/stboot/-/commit/334d268333f0ed7a4bda02cff561c07c7ed3c269
	//       https://git.glasklar.is/system-transparency/core/stprov/-/commit/e992f363ea53062029912b75b2b64d526b940ad5
	//
	// I.e., in the case of stprov ("Initial upload...") it was already in mpt.
	//
	// Monkey-patch as below if these fields are not explicitly set.
	FixedBondingMode = BondingMode(12345)
	FixedBondName    = "dummy-ifname-bonding"
)

func haveOneProvisioningURL(data []byte) error {
	cfg := struct {
		URLs *[]string `json:"provisioning_urls"`
	}{}

	if err := json.Unmarshal(data, &cfg); err != nil {
		return fmt.Errorf("monkey-patch: tried to parse \"provisioning_urls\" as list of strings")
	}
	if cfg.URLs == nil {
		return fmt.Errorf("monkey-patch: missing key \"provisioning_urls\"")
	}
	if n := len(*cfg.URLs); n != 1 {
		return fmt.Errorf("monkey-patch: \"provisioning_urls\" have length %d", n)
	}

	return nil
}

func haveNetworkInterface(data []byte) error {
	cfg := struct {
		NetworkInterface *netHardwareAddr `json:"network_interface"`
	}{}

	if err := json.Unmarshal(data, &cfg); err != nil {
		fmt.Printf("%s\n", string(data))
		return fmt.Errorf("monkey-patch: tried to fix \"network_interfaces\" from \"network_interface\": %v", err)
	}
	if cfg.NetworkInterface == nil {
		return fmt.Errorf("monkey-patch: missing key \"network_interface\"")
	}

	return nil
}

func haveInterfaceName(data []byte) error {
	var ifname string

	if err := json.Unmarshal(data, &ifname); err != nil {
		return fmt.Errorf("monkey_patch: \"network_interfaces\": tried to parse as list of string")
	}

	return nil
}

type DNSServers []*netIP

func (d *DNSServers) UnmarshalJSON(data []byte) error {
	type internal DNSServers
	if err := json.Unmarshal(data, (*internal)(d)); err != nil {
		var str string
		if err := json.Unmarshal(data, &str); err != nil {
			return fmt.Errorf("monkey-patch: tried to parse \"dns\" as a string")
		}

		srv := netIP(FixedDNS)
		*d = []*netIP{&srv}
	}
	return nil
}
